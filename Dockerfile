#FROM openjdk:8-jdk-alpine
#ADD ./build/libs/demo-0.0.1-SNAPSHOT.jar /spring/spring.jar
#ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "spring/spring.jar"]

FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY ./build/libs/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]

